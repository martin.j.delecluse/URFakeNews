"""
Module defining the "AI player"

It is a simple greedy player that will focus on the state that will yield the most votes
"""

from .geopolitics import State, REPUBLICANS, DEMOCRATS, RALLY_VALUE
from .resources import state_list

from helpers.rand import choice


def select_rally_place(country):
    """
    Greedy AI player

    List all the state that it can turn with a rally, then selects the one with the most electors.
    """
    attack_states = []  # States to attack in priority
    defense_states = []  # States to defend in priority
    other_states = []  # We need to go somewhere

    for state in country:  # type: State

        if state.votes.democrats > state.votes.republicans:
            # If opposite rally is unlikely to turn state, no need to consider it for own rally
            # (state is already won)
            if state.votes.democrats - RALLY_VALUE > state.votes.republicans + RALLY_VALUE:
                other_states.append((state.electors, state.abbrev))
            # If opposite rally COULD turn state, add it to list of states to defend
            else:
                defense_states.append((state.electors, state.abbrev))

        if state.votes.republicans > state.votes.democrats:
            # If rally is unlikely to turn state, no need to consider it for own rally
            # (state is already lost)
            if state.votes.republicans - RALLY_VALUE > state.votes.democrats + RALLY_VALUE:
                other_states.append((state.electors, state.abbrev))
            # If rally COULD turn state, add it to list of states to attack
            else:
                attack_states.append((state.electors, state.abbrev))

    attack_states.sort(reverse=True)
    defense_states.sort(reverse=True)
    other_states.sort(reverse=True)

    # Chose the state to rally to
    # Prioritize attack, then defense, then random
    state_choice = None
    if attack_states:
        state_choice = attack_states[0][1]
    elif defense_states:
        state_choice = defense_states[0][1]
    else:
        state_choice = other_states[0][1]
    return state_choice
