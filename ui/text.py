
from .colors import BLACK


def write_text(screen, text, pos, font, max_width, color=BLACK):  # copy pasta
    """
    write a text splitted over multiple lines, each line being shorter than max_width in width
    """
    words = [word.split(' ') for word in text.splitlines()]  # 2D array where each row is a list of words.
    space = font.size(' ')[0]  # The width of a space.
    x, y = pos
    for line in words:
        for word in line:
            word_surface = font.render(word, True, color)
            word_width, word_height = word_surface.get_size()
            if x + word_width >= max_width:
                x = pos[0]  # Reset the x.
                y += word_height  # Start on new row.
            screen.blit(word_surface, (x, y))
            x += word_width + space
        x = pos[0]  # Reset the x.
        y += word_height  # Start on new row.
#
#
#
# def write_info(text):
#     """
#     write info to the top right corner of the window
#     :param text:
#     :return:
#     """
#     if isinstance(text, gp.State):
#         info = str(text)
#     else:
#         info = text
#     info = info.replace("\t", "")
#     pygame.draw.rect(screen, background, pygame.Rect(info_x, 0, display_width - info_x, pos_map[1] - 2 * thickness))
#     write_text(info, (info_x, space), normal_font, display_width)
