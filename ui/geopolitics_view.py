"""
Definition of the geopolitics UI
"""

from pygame.image import load as load_img
from pygame.draw import rect as draw_rect, ellipse as draw_ellipse
from pygame import Rect

from .colors import BLACK, DARK_GRAY, MAP_GRAY, LIGHT_GREEN
from .fonts import SMALL_FONT
from .game_view import DISPLAY_WIDTH, DISPLAY_HEIGHT, MAP_OFFSET, THICKNESS

from gameplay.resources import geopolitics_info


# Parameters for 'state placeholders'
# They are used to represent states that are too tiny on the map
PLACEHOLDER_POS_X = DISPLAY_WIDTH - 45 - MAP_OFFSET[0]  # X pos of placeholders
PLACEHOLDER_POS_Y = DISPLAY_HEIGHT - 22 - MAP_OFFSET[1]  # Y pos of 1st placeholder; gaps are added afterwards
PLACEHOLDER_GAP = 38  # gap between 2 rectangles
PLACEHOLDER_WIDTH = 70
PLACEHOLDER_HEIGHT = 35

# Parameters for 'state buttons': buttons that user clicks on to interact with state gameplay element
BUTTON_X_OFFSET = 20
BUTTON_Y_OFFSET = 15
BUTTON_WIDTH = 40
BUTTON_HEIGHT = 30

BUTTON_LABEL_X_OFFSET = -17
BUTTON_LABEL_Y_OFFSET = -5


# Position of states (buttons) relative to the map
pos_state = {
    "AL": (695, 446),
    "AK": (110, 530),
    "AZ": (211, 397),
    "AR": (581, 411),
    "CA": (68, 291),
    "CO": (341, 304),
    "CT": (PLACEHOLDER_POS_X, PLACEHOLDER_POS_Y - 5 * PLACEHOLDER_GAP),
    "DE": (PLACEHOLDER_POS_X, PLACEHOLDER_POS_Y - 2 * PLACEHOLDER_GAP),
    "DC": (PLACEHOLDER_POS_X, PLACEHOLDER_POS_Y),
    "FL": (820, 557),
    "GA": (764, 448),
    "HI": (317, 606),
    "ID": (204, 171),
    "IL": (632, 275),
    "IN": (688, 277),
    "IA": (566, 236),
    "KS": (468, 321),
    "KY": (725, 331),
    "LA": (581, 475),
    "ME": (950, 97),
    "MD": (PLACEHOLDER_POS_X, PLACEHOLDER_POS_Y - PLACEHOLDER_GAP),
    "MA": (PLACEHOLDER_POS_X, PLACEHOLDER_POS_Y - 6 * PLACEHOLDER_GAP),
    "MI": (704, 205),
    "MN": (542, 144),
    "MS": (638, 457),
    "MO": (580, 323),
    "MT": (297, 104),
    "NE": (449, 247),
    "NV": (135, 258),
    "NH": (PLACEHOLDER_POS_X, PLACEHOLDER_POS_Y - 7 * PLACEHOLDER_GAP),
    "NJ": (PLACEHOLDER_POS_X, PLACEHOLDER_POS_Y - 3 * PLACEHOLDER_GAP),
    "NM": (323, 404),
    "NY": (864, 176),
    "NC": (833, 363),
    "ND": (444, 104),
    "OH": (748, 263),
    "OK": (488, 392),
    "OR": (103, 140),
    "PA": (835, 237),
    "RI": (PLACEHOLDER_POS_X, PLACEHOLDER_POS_Y - 4 * PLACEHOLDER_GAP),
    "SC": (809, 411),
    "SD": (440, 180),
    "TN": (696, 378),
    "TX": (459, 489),
    "UT": (231, 286),
    "VT": (PLACEHOLDER_POS_X, PLACEHOLDER_POS_Y - 8 * PLACEHOLDER_GAP),
    "VA": (835, 315),
    "WA": (144, 66),
    "WV": (785, 305),
    "WI": (616, 176),
    "WY": (316, 205),
}


class CountryView:
    """ View of the 'Country' geopolitics element """

    def __init__(self, country, screen, position):
        # Link to gameplay
        self._country = country

        # Screen to draw to & drawing offset
        self._screen = screen
        self._position = position

        # State views children
        self._init_state_views()

        # UI constants // could be improved to not have to handle offset ourselves
        self._map_image = load_img('res/img/usa_map_gray.png')

        # Map outline
        self._outline_rect = (
            position[0] - THICKNESS,
            position[1] - THICKNESS,
            DISPLAY_WIDTH - position[0] + 2 * THICKNESS,
            DISPLAY_HEIGHT - position[1] + 2 * THICKNESS
        )
        self._placeholder_rect_list = []
        for i in range(9):  # There are 9 placeholder for tiny states
            self._placeholder_rect_list.append(
                Rect(
                    DISPLAY_WIDTH - 80,
                    DISPLAY_HEIGHT - 40 - i * PLACEHOLDER_GAP,
                    PLACEHOLDER_WIDTH,
                    PLACEHOLDER_HEIGHT
                )
            )

    def _init_state_views(self):
        self._state_view_list = []

        for state_abbrev, state_offset in pos_state.items():
            state_position = (
                self._position[0] + state_offset[0],
                self._position[1] + state_offset[1]
            )

            state_view = StateView(
                self._country.states[state_abbrev],
                self._screen,
                state_position
            )
            self._state_view_list.append(state_view)

    def draw(self):
        """ Draw the geopolitics map and the states (incl. placeholders) """
        self._draw_map()
        for state_view in self._state_view_list:
            state_view.draw()

    def _draw_map(self):
        """ Draw the geopolitics map """
        draw_rect(self._screen, DARK_GRAY, self._outline_rect, THICKNESS)
        self._screen.blit(self._map_image, self._position)

        # draw rectangle 'placeholders' for the states that are too tiny to fit on the map
        for placeholder_rect in self._placeholder_rect_list:
            draw_rect(self._screen, MAP_GRAY, placeholder_rect)

    def click(self, click_position):
        for state_view in self._state_view_list:
            if state_view.click(click_position):
                return state_view.state


class StateView:
    """ View of the 'State' geopolitics element """

    def __init__(self, state, screen, position):
        # Link to gameplay
        self.state = state

        # Screen to draw to & drawing offset
        self._screen = screen
        self._position = position

        # Init the button ui elements
        self._button_rect = Rect(
            self._position[0] - BUTTON_X_OFFSET,
            self._position[1] - BUTTON_Y_OFFSET,
            BUTTON_WIDTH,
            BUTTON_HEIGHT
        )
        # Init the label ui elements
        self._label_text = f"{state.abbrev}: {geopolitics_info[state.abbrev]}"
        self._label_surf = SMALL_FONT.render(self._label_text, True, BLACK)
        self._label_rect = self._label_surf.get_rect().move(
            self._position[0] + BUTTON_LABEL_X_OFFSET,
            self._position[1] + BUTTON_LABEL_Y_OFFSET
        )

    def draw(self):
        self._draw_button()
        self._draw_label()

    def _draw_button(self):
        """ Draw the button (ellipse) """
        # Use ellipse to fit more text in label
        draw_ellipse(self._screen, LIGHT_GREEN, self._button_rect)

    def _draw_label(self):
        """ Draw the label: 'state_abbrev: electors' inside the button """
        self._screen.blit(self._label_surf, self._label_rect)

    def click(self, click_position):
        return self._button_rect.collidepoint(click_position)
