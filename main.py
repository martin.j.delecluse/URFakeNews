
import pygame
pygame.init()
import sys

from pygame.locals import *

# Must be the 1st call
pygame.init()

from ui.game_view import GameView
from gameplay.game import Game


def main():
    game = Game()
    game_view = GameView(game)

    game_view.draw()

    pygame.display.set_caption('U R Fake News')

    game_view.load_game_ui()

    soundtrack = pygame.mixer.Sound("res/sound/soundtrack.wav")
    twitter_sound = pygame.mixer.Sound('res/sound/twitter_sound.wav')
    soundtrack.play(loops=-1)
    soundtrack.set_volume(0.25)

    while True:
        # update map
        pygame.display.update()

        ev = pygame.event.get()
        for event in ev:

            # handle click
            if event.type == pygame.MOUSEBUTTONUP:
                click_pos = pygame.mouse.get_pos()
                game_view.click(click_pos)

            if event.type == QUIT:
                pygame.quit()
                sys.exit()

            game_view.draw()

        # 1st step of donald's turn: fake news

        # 2nd step of donald's turn: meeting

        # biden's turn


def test():
    from gameplay.game import Game
    from gameplay.geopolitics import REPUBLICANS, DEMOCRATS

    wins = {REPUBLICANS: 0, DEMOCRATS: 0}
    for x in range(1000):
        game = Game()
        for t in range(10):
            game.take_turn()
        game.stop_the_count()
        results = game.get_vote_count()

        winner = REPUBLICANS if results[REPUBLICANS] > results[DEMOCRATS] else DEMOCRATS
        wins[winner] += 1
    print(wins)


if __name__ == "__main__":
    main()
    # test()